var express = require('express'),
  cartRouter = express.Router();

var cartController = require('../controllers/cart').getInstance();

module.exports = function () {
  cartRouter.get('/', cartController.getAllCart);
  cartRouter.post('/', cartController.addProductToCart);
  cartRouter.delete('/', cartController.removeProductFromCart);
  return cartRouter;
};
