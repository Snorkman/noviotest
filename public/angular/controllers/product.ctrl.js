(function() {
  'use strict';

  angular
    .module('commonApp')
    .controller('productCtrl', productCtrl);

  productCtrl.$inject = ['ProductsService', 'CartService', 'BroadcastService', '$mdToast'];

  function productCtrl (productsService, CartService, BroadcastService, $mdToast) {
    var vm = this;
    vm.bannerImages = _.range(1, 13);

    vm.addToCart = addToCart;
    vm.removeFromCart = removeFromCart;

    vm.checkProductsInCart = function (cart) {
      _.each(vm.products, function (product) {
        var chossenProduct = _.find(cart.products, ['_id', product._id]);
        var chossenProductExist = !_.isUndefined(chossenProduct);
        product.isShowDelete = chossenProductExist;
        product.isShowAdd = !chossenProductExist || 10 - chossenProduct.quantity >= 1;
        product.valueToAdd = 1;
        if (product.isShowDelete) {
          product.ranges = _.range(1, 11 - chossenProduct.quantity);
        } else {
          product.ranges = _.range(1, 11);
        }
      });
    };

    productsService.getAllProducts()
      .then(function (products) {
        vm.products = products;
        CartService.getAllCart()
          .then(function (result) {
            vm.cart = result;
            vm.checkProductsInCart(vm.cart);
            BroadcastService.send('cartIsChanged', vm.cart);
          })
          .catch(function(error) {
            $mdToast.show(
              $mdToast.simple()
                .textContent(error)
                .position('bottom right')
                .hideDelay(3000)
            );
          });
      });

    function addToCart (product) {
      CartService.addProductToCart(product)
        .then(function (result) {
          vm.cart = result;
          vm.checkProductsInCart(vm.cart)
          BroadcastService.send('cartIsChanged', vm.cart);
        })
        .catch(function(error) {
          $mdToast.show(
            $mdToast.simple()
              .textContent(error)
              .position('bottom right')
              .hideDelay(3000)
          );
        });
    }

    function removeFromCart (product) {
      CartService.removeProductFromCart(product)
        .then(function (result) {
          vm.cart = result;
          vm.checkProductsInCart(vm.cart)
          BroadcastService.send('cartIsChanged', vm.cart);
        })
        .catch(function(error) {
          $mdToast.show(
            $mdToast.simple()
              .textContent(error)
              .position('bottom right')
              .hideDelay(3000)
          );
        });
    }

  }
})();