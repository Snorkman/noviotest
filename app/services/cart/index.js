var _ = require('lodash'),
  Bluebird = require('bluebird');
var _instance;

var Cart = function () {};

var recalculateCart = function (cart) {
  if (cart) {
    var sum = 0;
    _.forEach(cart.products, function (value) {
      sum = sum + (value.price * value.quantity);
    });
    return sum;
  }
};

Cart.prototype.addProductToCart = function (product, cart, quantity) {
  quantity = Number(quantity);
  return new Bluebird(function (resolve, reject) {
    var cartProduct = _.find(cart.products, function (currentProduct) {
      return currentProduct._id == product._id;
    });
    if (cartProduct) {
      cartProduct.quantity = Number(cartProduct.quantity);
      var isMaxQuantityExeed = cartProduct.quantity > 10 ||
         (cartProduct.quantity + quantity) > 10;
      if (isMaxQuantityExeed) {
        reject(new Error('Превышен лимит на продукт'));
      } else {
        cartProduct.quantity = cartProduct.quantity + quantity;
      }
    } else {
      var newProduct = {
        _id: product._id,
        name: product.name,
        description: product.description,
        price: product.price,
        quantity: quantity
      };
      cart.products.push(newProduct)
    }
    cart.totalSum = recalculateCart(cart);
    cart.productsCount = cart.products.length;
    resolve(cart);
  });
};

Cart.prototype.removeProductFromCart = function (product, cart) {
  var cartProduct = _.find(cart.products, function (currentProduct) {
    return currentProduct._id == product._id;
  });
  if (cartProduct.quantity === 1) {
    _.remove(cart.products, cartProduct);
  } else {
    cartProduct.quantity--;
  }
  cart.totalSum = recalculateCart(cart);
  cart.productsCount = cart.products.length;
  return cart;
};

Cart.prototype.getAllCart = function (req) {
  return new Bluebird(function (resolve, reject) {
    resolve(req.session.cart || {});
  });
};


module.exports = {
  getInstance: function () {
    if (!_instance) {
      _instance = new Cart();
    }
    return _instance;
  }
};