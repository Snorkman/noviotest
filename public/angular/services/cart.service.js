(function() {
  'use strict';

  angular
    .module('commonApp')
    .factory('CartService', CartService);

  CartService.$inject = ['$http', '$q'];

  function CartService ($http, $q) {

    var that = this;

    that.addProductToCart = function (product) {
      var deferred = $q.defer();
      $http.post('api/cart', { productId: product._id, quantity: product.valueToAdd })
        .success(function(result) {
          deferred.resolve(result);
        })
        .error(function(err) {
          deferred.reject(err);
        });
      return deferred.promise;
    };

    that.removeProductFromCart = function (product) {
      var deferred = $q.defer();
      $http.delete('api/cart/?productId='+product._id)
        .success(function(result) {
          deferred.resolve(result);
        })
        .error(function(err) {
          deferred.reject(err);
        });
      return deferred.promise;
    };

    that.getAllCart = function () {
      var deferred = $q.defer();
      $http.get('api/cart')
        .success(function(result) {
          deferred.resolve(result);
        })
        .error(function(err) {
          deferred.reject(err);
        });
      return deferred.promise;
    };

    return that;
  }
})();