(function() {
  'use strict';

  angular
    .module('commonApp')
    .factory('BroadcastService', BroadcastService);

  BroadcastService.$inject = ['$rootScope'];

  function BroadcastService ($rootScope) {

    var that = this;

    that.send = function (event, data) {
      $rootScope.$broadcast(event, data);
    };

    return that;
  }
})();