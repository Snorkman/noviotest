var productModel = require('../../models/product.model');
// var productDataProvider = require('../../dataAccess/productDataProvider/index').getInstance();

var _instance;

var Products = function () {};

/********
Function to retrieve all products
********/

Products.prototype.getAllProducts = function () {
    return productModel.find(function(err, product) {
      if (err) {
        throw new Error(err);
      } else {
        return product || [];
      }
    });
};

Products.prototype.getProduct = function (productId) {
    return productModel.findById(productId, function(err, product) {
      if (err) {
        throw new Error(err);
      } else {
        return product || [];
      }
    });
};

module.exports = {
  getInstance: function () {
    if (!_instance) {
      _instance = new Products();
    }

    return _instance;
  }
};