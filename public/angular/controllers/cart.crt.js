(function() {
  'use strict';

  angular
    .module('commonApp')
    .controller('cartCtrl', cartCtrl);

  cartCtrl.$inject = ['CartService', 'BroadcastService'];

  function cartCtrl (CartService, BroadcastService) {
    var vm = this;

    CartService.getAllCart()
      .then(function (result) {
        vm.cart = result;
      });
  }
})();