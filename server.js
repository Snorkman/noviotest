var express = require("express"),
		path = require("path"),
		http = require("http"),
		cookieParser = require("cookie-parser"),
		logger = require("morgan"),
		bodyParser = require("body-parser"),
    mongoose   = require('mongoose'),
    session = require('express-session');

var Routes = require("./app/routes");

var app = express();
var server = http.createServer(app);
var routes = new Routes();

mongoose.connect('mongodb://test:test@ds055895.mongolab.com:55895/testtask');

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ 
    secret: 'shoppingCart',
    proxy: true,
    resave: true,
    saveUninitialized: true,
     cookie: { 
      maxAge: 300000 
    }
  }));

app.use(express.static(path.join(__dirname, '/public')));
app.use('/bower_components', express.static(__dirname + '/public/bower_components'));
app.set('views', path.join(__dirname, '/public/views'));
app.set('view engine', 'ejs');

app.use("/", routes);

//For avoidong Heroku $PORT error
app.get('/', function(request, response) {
    var result = 'App is running'
    response.send(result);
}).listen(app.get('port'), function() {
    console.log('App is running, server is listening on port ', app.get('port'));
});

server.listen(process.env.PORT ||5000, function (err) {
  if (err) {
    console.err('Error while starting server...', err);
  } else {
    console.info('Server started and listening on Port:', 5000);
  }
});
