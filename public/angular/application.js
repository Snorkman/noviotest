(function() {
  'use strict';

  angular
    .module('commonApp', [
      'ngMaterial',
      'ui.router',
      'ngAnimate'
    ])
    .config(config)
    .run(run);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];
  run.$inject = ['$rootScope', '$state'];

  function config ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/products');

    $stateProvider
      .state('base', {
        url: '',
        abstract: true,
        views: {
          'header': {
            templateUrl: 'header',
            controller: 'HeaderCtrl',
            controllerAs: 'header'
          }
        }
      })
      .state('base.products', {
        url: "/products",
        views: {
          'container@': {
            templateUrl: "products",
            controller: "productCtrl",
            controllerAs: "productAs"
          }
        }
      })
      .state('base.cart', {
        url: "/cart",
        views: {
          'container@': {
            templateUrl: "cart",
            controller: "cartCtrl",
            controllerAs: "cartAs"
          }
        }
      });
  }

  function run ($rootScope, $state, $cookies, $location) {
    $rootScope.$state = $state;
  }
})();