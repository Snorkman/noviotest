var gulp = require('gulp'),
	watch = require('gulp-watch'),
	jshint = require('gulp-jshint'),
	gulpif = require('gulp-if'),
	stripDebug = require('gulp-strip-debug'),
	removeLogs = require('gulp-removelogs'),
	ngmin = require('gulp-ngmin'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	concatCss = require('gulp-concat-css'),
	plumber = require('gulp-plumber'),
	stylus = require('gulp-stylus'),
	bs = require('browser-sync'),
	server = require('gulp-develop-server'),
	runSequence = require('run-sequence');

var reload = bs.reload;

var serverFiles = ['./app/{,**/*/}*.js'],
	clientFiles = ['./public/angular/{,**/*/}*.js'],
	cssFiles = ['./public/stylesheets/{,**/*/}*.css'],
	htmlFiles = ['./public/views/{,**/*/}*.ejs'];

var options = {
	server: {
		path: './server.js',
		execArgv: ['--harmony']
	},
	bs: {
		proxy: 'http://localhost:8000/'
	}
};

gulp.task( 'server:start', function() {
	server.listen( options.server, function( error ) {
		if(!error) bs(options.bs);
	});
});

gulp.task( 'server:restart', function() {
	server.restart( function( error ) {
		if(!error) bs.reload();
	});
});

gulp.task('set-dev', function() {
	env = 'dev';
});

gulp.task('set-prod', function() {
	env = 'prod';
});

gulp.task('js', function () {
	return gulp.src(clientFiles)
		.pipe(gulpif(env === 'prod', removeLogs()))
		.pipe(plumber())
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(concat('index.js'))
		.pipe(gulpif(env === 'prod',stripDebug()))
		.pipe(gulpif(env === 'prod',ngmin()))
		.pipe(gulp.dest('./public/build/js'))
		.pipe(gulpif(env === 'prod', uglify({mangle: false})))
		.pipe(gulpif(env === 'prod', gulp.dest('./public/build/js')));
});

gulp.task('css', function () {
	return gulp.src(cssFiles)
		.pipe(plumber())
		.pipe(stylus())
		.pipe(concatCss('index.css'))
		.pipe(gulp.dest('./public/build/css'));
});

gulp.task('jshintServer', function () {
	return gulp.src(serverFiles)
		.pipe(plumber())
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('watch', function () {
	gulp.watch(clientFiles, ['js', reload]);
	gulp.watch(serverFiles, ['jshintServer', 'server:restart']);
	gulp.watch(serverFiles, ['server:restart']);
	gulp.watch(cssFiles, ['css', reload]);
	gulp.watch(htmlFiles, reload);
});

gulp.task('dev', function() {
	runSequence(
		'set-dev',
		['js',
		'css',
		'jshintServer',
		'server:start',
		'watch']
	);
});

gulp.task('prod', function(callback) {
	runSequence(
		'set-prod',
		['js',
		'css',
		'jshintServer']
	);
});
