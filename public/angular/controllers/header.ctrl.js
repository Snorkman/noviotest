(function() {
  'use strict';

  angular
    .module('commonApp')
    .controller('HeaderCtrl', HeaderCtrl);

 HeaderCtrl.$inject = ['$rootScope'];

  function HeaderCtrl ($rootScope) {
    var vm = this;
    vm.productsCount = 0;

    $rootScope.$on("cartIsChanged", function (event, cart) {
      vm.productsCount = cart.productsCount;
    });

  }
})();