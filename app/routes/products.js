var express = require('express'),
  productsRouter = express.Router();

var productsController = require('../controllers/products').getInstance();

module.exports = function () {
  productsRouter.get('/', productsController.getAllProducts);
  return productsRouter;
};
