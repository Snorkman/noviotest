var constants = require('./constants');

var ControllerHelper = function (res) {

  var sendErrorResponse = function (err) {
    res.status(err.code || constants.httpCodes.internalServerError).send(err.message);
  };

  var sendResponse = function (code, result) {
    res.status(code || constants.httpCodes.success).send(result);
  };

  return {
    sendResponse: sendResponse,
    sendErrorResponse: sendErrorResponse
  };
};

module.exports = ControllerHelper;
