var _instance;

var ControllerHelper = require('../../common/controllerHelper'),
  productsService = require('../../services/products').getInstance(),
  constants = require('../../common/constants');

var Products = function () {};

/*****
Function to retrieve a list of existing products
*****/

Products.prototype.getAllProducts = function (req, res) {
  var controllerHelper = new ControllerHelper(res);
  productsService.getAllProducts()
    .then(function (result) {
      controllerHelper.sendResponse(constants.httpCodes.success, result);
    })
    .catch(function (err) {
      controllerHelper.sendErrorResponse(err);
    });
};

module.exports = {
  getInstance: function () {
    if (!_instance) {
      _instance = new Products();
    }
    return _instance;
  }
};