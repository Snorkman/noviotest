var _ = require('lodash');

var _instance;

var ControllerHelper = require('../../common/controllerHelper'),
  cartService = require('../../services/cart').getInstance(),
  productsService = require('../../services/products').getInstance(),
  constants = require('../../common/constants');

var Cart = function () {};

/*****
Function to retrieve a list of existing cart
*****/

Cart.prototype.getAllCart = function (req, res) {
  var controllerHelper = new ControllerHelper(res);
  cartService.getAllCart(req)
    .then(function (result) {
      console.log('result',result);
      controllerHelper.sendResponse(constants.httpCodes.success, result);
    })
    .catch(function (err) {
      controllerHelper.sendErrorResponse(err);
    });
};

Cart.prototype.addProductToCart = function (req, res) {
  var controllerHelper = new ControllerHelper(res);
  var productId = req.body.productId;
  var quantity = req.body.quantity;
  productsService.getProduct(productId)
    .then(function (result) {
      if (result) {
        if (Number(quantity) > 10) {
          var error = new Error('Количество продукта превышает допустимое значение');
          error.code = constants.httpCodes.badRequest;
          throw error;
        } else {
          req.session.cart = req.session.cart || {};
          req.session.cart.products = req.session.cart.products || [];
          req.session.cart.totalSum = req.session.cart.totalSum || 0;
          req.session.cart.productsCount = req.session.cart.productsCount || 0;
          var cart = req.session.cart;
          return cartService.addProductToCart(result, cart, quantity)
        }
      } else {
        var error = new Error('Такого продукта нет в системе');
        error.code = constants.httpCodes.badRequest;
        throw error;
      }
    })
    .then(function (result) {
      controllerHelper.sendResponse(constants.httpCodes.success, result);
    })
    .catch(function (err) {
      controllerHelper.sendErrorResponse(err);
    });
};

Cart.prototype.removeProductFromCart = function (req, res) {
  var controllerHelper = new ControllerHelper(res);
  var cart = req.session.cart;
  if (!_.isUndefined(cart) && cart.products.length > 0) {
    var productId = req.query.productId;
    productsService.getProduct(productId)
      .then(function (result) {
        if (result) {
          return cartService.removeProductFromCart(result, cart)
        } else {
          var error = new Error('Такого продукта нет в системе');
          error.code = constants.httpCodes.badRequest;
          throw error;
        }
      })
      .then(function (result) {
        controllerHelper.sendResponse(constants.httpCodes.success, result);
      })
      .catch(function (err) {
        controllerHelper.sendErrorResponse(err);
    });
  } else {
    controllerHelper.sendResponse(constants.httpCodes.badRequest, 'Корзина пуста');
  }
};

module.exports = {
  getInstance: function () {
    if (!_instance) {
      _instance = new Cart();
    }
    return _instance;
  }
};