'use strict';
var express = require('express'),
    router = express.Router();

var ProductsRouter = require('./products');
var CartRouter = require('./cart');

module.exports = function () {
  var productsRouter = new ProductsRouter();
  var cartRouter = new CartRouter();

  router.get('/', function (req, res) {
    res.render('index');
  });

  router.get('/products', function (req, res) {
    res.render('pages/products');
  });

  router.get('/cart', function (req, res) {
    res.render('pages/cart');
  });

  router.get('/header', function (req, res) {
    res.render('pages/top_bar');
  });

  router.use('/api/cart', cartRouter);
  router.use('/api/products', productsRouter);

  return router;
};