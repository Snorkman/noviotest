(function() {
  'use strict';

  angular
    .module('commonApp')
    .factory('ProductsService', ProductsService);

  ProductsService.$inject = ['$http', '$q'];

  function ProductsService ($http, $q) {

    var that = this;

    that.getAllProducts = function  () {
      var deferred = $q.defer();
      $http.get('api/products')
        .success(function(result) {
          deferred.resolve(result);
        })
        .error(function(err) {
          deferred.reject(err);
        });
      return deferred.promise;
    };

    return that;
  }
})();